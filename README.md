# DEWS
DEWS (DEep White matter hyperintensity Segmentation framework) \
: A fully automated pipeline for detecting small deep white matter hyperintensities in migraineurs 

* **Please cite the paper if you use this code** \
*B.-y. Park and M. J. Lee et al.* DEWS (DEep White matter hyperintensity Segmentation framework): A fully automated pipeline for detecting small deep white matter hyperintensities in migraineurs. *NeuroImage: Clinical* 18:638-647 (2018). \
https://www.sciencedirect.com/science/article/pii/S2213158218300676

# Prerequisite
* DEWS requries MATLAB in linux

* AFNI and FSL should be installed before running the program

* Please visit the web-sites below for AFNI and FSL \
**AFNI:** https://afni.nimh.nih.gov/download \
**FSL:** https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation

* The required versions are MATLAB 2016b, AFNI after 1998, and FSL 5.0.8

# Versions
* **version1-1:** Detecting WMHs using intensity-based threshold and region growing approach

* **version1-2:** Detecting WMHs using Gaussian mixture model (GMM) distribution clustering algorithm

# Downlad link
https://github.com/bypark/DEWS \
If you encounter an error while opening the source code, please directly contact to the author: by9433@gmail.com


# Websites

* **LAB (CAMIN: Computational Analysis for Multimodal Integrative Neuroimaging):** https://www.caminlab.com/
* **LAB (MIPL: Medical Image Processing Lab):** https://mipskku.wixsite.com/mipl